package com.pharmacripter.eneko.pharmacripter;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";


    NfcAdapter nfcAdapter;
    ArrayList<Alarm> alarms = new ArrayList<Alarm>();
    private static final String TAG = MainActivity.class.getName();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        create_time_thread();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC available", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "NFC not available", Toast.LENGTH_LONG).show();
            finish();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void create_time_thread() {
        Thread t = new Thread() {



            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {

                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                update_alarms();
                                update_time_visualization();
                                update_alarms_visualization();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();
    }

    public void settings_function(View view) {
        Log.i("***********", "settings fn");
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        launchNotification(0);

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();
        Toast.makeText(this, NfcAdapter.ACTION_TECH_DISCOVERED, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, action, Toast.LENGTH_SHORT).show();
        Log.i(TAG, "tag discovered");

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
            Toast.makeText(this, "new nfc intent", Toast.LENGTH_LONG).show();
            ArrayList<Alarm> tag_alarms = new ArrayList<Alarm>();
            Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            MifareClassic mfc = MifareClassic.get(tagFromIntent);
            byte[] data;
            try {
                mfc.connect();
                boolean auth = false;
                int secCount = mfc.getSectorCount();
                int bCount = 0;
                int bIndex = 0;
                for (int j = 0; j < secCount; j++) {
                    auth = mfc.authenticateSectorWithKeyA(j, MifareClassic.KEY_DEFAULT);
                    if (auth) {
                        bCount = mfc.getBlockCountInSector(j);
                        bIndex = 0;
                        for (int i = 0; i < bCount; i++) {
                            bIndex = mfc.sectorToBlock(j) + i;
                            Log.i("eneko", "sector to block " + bIndex);
                            data = mfc.readBlock(bIndex);
                            if (Integer.valueOf(data[0]) != 0) {
                                String name = new String(data, 0, 10);
                                String qty = String.valueOf(Integer.valueOf(data[10])) + new String(data, 11, 3);
                                String time = String.valueOf(Integer.valueOf(data[14]) * 10 + Integer.valueOf(data[15]));
                                tag_alarms.add(new Alarm(name, qty, time));
                                Log.i(TAG, "NEW ALARM ADDED " + tag_alarms.get(tag_alarms.size() - 1));
                                data[0]=0x6F;
                                //data[0]=0;
                                mfc.writeBlock(bIndex, data);
                            }
                            Log.i(TAG, Utils.getHexString(data));
                            bIndex++;
                        }
                    } else {
                        Log.i(TAG, "Auth. failed in sector " + j);
                    }
                }
                if (tag_alarms.size() != 0) {
                    //BORRATU MEMORIA
                    alarms.clear();
                    alarms = (ArrayList) tag_alarms.clone();
                    update_alarms_visualization();
                }
            } catch (IOException e) {

            }

            update_alarms_visualization();
        }

    }

    private void update_layout(){
        update_time_visualization();
        update_alarms_visualization();
        ImageView image = (ImageView) findViewById(R.id.logo);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            image.setVisibility(View.VISIBLE);
        } else {
            image.setVisibility(View.GONE);
        }

    }

    private void update_time_visualization(){
        TextView time = (TextView) findViewById(R.id.actual_t);
        TextView next = (TextView) findViewById(R.id.next_alarm);
        Calendar calendar = Calendar.getInstance();
        int seconds = calendar.get(Calendar.SECOND);
        int minutes = calendar.get(Calendar.MINUTE);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        time.setText(String.valueOf(hours) + ":" + String.valueOf(minutes) + ":" + String.valueOf(seconds));
        if (alarms.size() != 0) {
            next.setText(alarms.get(0).getFormattedTime());
            next.setBackgroundResource(R.drawable.shape_rounded);
        } else {
            next.setText("ACTUALIZAR");
            next.setBackgroundResource(R.drawable.shape_rounded_error);
        }

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            time.setText("\n"+time.getText()+"\n");
            next.setText("\n"+next.getText()+"\n");
        }
    }

    private void update_alarms() {
        int i = 0;
        while (i < alarms.size()) {
            if (alarms.get(i).remaining_time == 0) {
                launchNotification(i);
                alarms.remove(i);
                Log.i("eneko::::", "alarm " + i + " removed");
            } else {
                alarms.get(i).update_remaining(-1);
                i++;
            }
        }
    }

    private void update_alarms_visualization() {
        GridView gridView = (GridView) findViewById(R.id.info_table);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);


        final ArrayList<String> info = new ArrayList<String>();
        for (int i = 0; i < alarms.size(); i++) {
            info.add(alarms.get(i).getName());
            info.add(alarms.get(i).getQty());
            info.add(alarms.get(i).getFormattedTime());
        }

        assert gridView != null;
        gridView.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, info) {
            public View getView(int position, View convertView, ViewGroup parent) {

                // Return the GridView current item as a View
                View view = super.getView(position, convertView, parent);

                // Convert the view as a TextView widget
                TextView tv = (TextView) view;

                //tv.setTextColor(Color.DKGRAY);

                // Set the layout parameters for TextView widget
                LayoutParams lp = new LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
                );
                tv.setLayoutParams(lp);

                // Get the TextView LayoutParams
                LayoutParams params = (LayoutParams) tv.getLayoutParams();

                // Set the width of TextView widget (item of GridView)
                params.width = getPixelsFromDPs(MainActivity.this, 168);

                // Set the TextView layout parameters
                tv.setLayoutParams(params);

                // Display TextView text in center position
                tv.setGravity(Gravity.CENTER);

                // Set the TextView text font family and text size
                tv.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                // Set the TextView text (GridView item text)
                tv.setText(info.get(position));

                if((position/3)%2 == 0){
                    tv.setBackgroundColor(Color.LTGRAY);
                }else{
                    tv.setBackgroundColor(0x00000000);
                }

                tv.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));

                // Set the TextView background color
                //tv.setBackgroundColor(Color.parseColor("#FF82DE81"));

                // Return the TextView widget as GridView item
                return tv;
            }
        });
    }

    public static int getPixelsFromDPs(Activity activity, int dps) {
        Resources r = activity.getResources();
        int px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }

    private void launchNotification(int id){
        long [] vibrationPattern = {500,1000,500,1000,500};
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_logo)
                        .setContentTitle("NUEVA TOMA: "+alarms.get(id).getName())
                        .setContentText("Dosis: "+alarms.get(id).getQty())
                        .setLights(2,1000,1000)
                        .setVibrate(vibrationPattern)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        // Sets an ID for the notification
        int mNotificationId = 001;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //Calendar calendar = Calendar.getInstance();
        ArrayList<String> alarms_string = new ArrayList<String>();
        for(int i=0; i<alarms.size(); i++) {
            alarms_string.add(alarms.get(i).toString());
        }
        savedInstanceState.putStringArrayList("alarms_string", alarms_string);
        Log.i("***", "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSAVEDDDDDDDDDDDDDDDDDDD");
        //savedInstanceState.putIntegerArrayList("actual_timestamp", calendar.getTimeInMillis());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("***", "RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRESTOREDDDDDDD");
        ArrayList<String> alarms_string = savedInstanceState.getStringArrayList("alarms_string");
        for (int i = 0; i < alarms_string.size(); i++) {
            alarms.add(new Alarm(alarms_string.get(i)));
        }

        update_alarms_visualization();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("+++", "***********************************************************************onresume");
        update_layout();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilter = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilter, null);
    }

    @Override
    protected void onPause() {
        Log.i("+++","***********************************************************************onpause");
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onStart() {
        Log.i("+++","***********************************************************************onstart");
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.pharmacripter.eneko.pharmacripter/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.pharmacripter.eneko.pharmacripter/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}


