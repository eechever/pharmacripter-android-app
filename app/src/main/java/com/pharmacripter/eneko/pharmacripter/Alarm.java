package com.pharmacripter.eneko.pharmacripter;

import android.util.Log;

/**
 * Created by Eneko on 28/05/2016.
 */
public class Alarm {

    String name, qty, time;
    int remaining_time;

    Alarm(String name, String qty, String time){
        this.name=name;
        this.qty=qty;
        this.time=time;
        remaining_time=Integer.valueOf(time);
        update_remaining(0);
    }

    Alarm(String toString){
        name = toString.substring(toString.indexOf("Name: ") + 6, toString.indexOf("\nQty"));
        Log.i("eneko:::", "name: " + name);
        qty = toString.substring(toString.indexOf("Qty: ") + 5, toString.indexOf("\nTime"));
        Log.i("eneko:::", "qty: " + qty);
        time = toString.substring(toString.indexOf("Time: ") + 6, toString.indexOf(" minutes"));
        Log.i("eneko:::", "time: " + time);
        remaining_time = Integer.valueOf(time);
    }

    public void update_remaining(int diff_min){
        remaining_time+=diff_min;
    }

    public String getName() {
        return name;
    }

    public String getQty() {
        return qty;
    }

    public String getTime() {
        return time;
    }

    public int getRemaining_time() {
        return remaining_time;
    }

    public String getFormattedTime() {
        String formatted_time;

        int cpt = Integer.valueOf(remaining_time);
        int days = cpt/60/24;
        int hours = (cpt-days*24*60)/60;
        int minutes = cpt - days*24*60 - hours*60;

        if(days==0){
            if(hours==0){
                formatted_time=new String(minutes+"m");
            }else {
                formatted_time=new String(hours+"h:"+minutes+"m");
            }
        }else{
            formatted_time=new String(days+"d::"+hours+"h:"+minutes+"m");
        }

        return formatted_time;
    }

    public String toString(){
        String alarm_str = new String("\nName: "+name+"\nQty: "+qty+"\nTime: "+remaining_time+" minutes");
        return alarm_str;
    }

}
